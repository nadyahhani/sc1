import java.util.ArrayList;

/**
 * player 1 = current player = 1
 */

public class Reversi {
    Board board;
    int currentPlayer;
    int turns;
    public void Othello(int size){
        board.Board(size);
        currentPlayer = 1;
        turns = 1;
    }

    public ArrayList<Board> getValidMoves(){
        ArrayList<Board> validMoves = new ArrayList<Board>();
        Board b = new Board();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {

                //board b = new board(cloneGrid(gamestate));

                if (b.check(j, i, currentPlayer)) {
                    validMoves.add(b);
                    b = new Board(); // if it would've been false it can be reused
                }
            }
        }

        return validMoves;
    }

    public void move(int x, int y){
        board.state[x][y] = currentPlayer;
        turns++;
    }
    public void setCurrentPlayer(){
        if (turns % 2 == 0){
            currentPlayer = 2;
        }
        else{
            currentPlayer = 1;
        }
    }

}
