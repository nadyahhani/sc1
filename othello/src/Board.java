public class Board {
    int size;
    int[][] state = new int[size][size];
    public void Board(int size){
        this.size = size;
        for (int i = 0; i < size; i++ ){
            for (int j = 0; j < size ; j++){
                this.state[i][j] = 0;
            }
        }
    }

    public boolean check(int x, int y, int player){
        if (state[x][y] != 0)
            return false;

        boolean legalAtLeastOnce = false;

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0)
                    continue;

                boolean piecesToFlip = false, passedOpponent = false;
                int k = 1;

                while (x + j * k >= 0 && x + j * k < size
                        && y + i * k >= 0 && y + i * k < size) {

                    if (state[x + j * k][y + i * k] == 0 ||
                            (state[x + j * k][y + i * k] == player && !passedOpponent))
                    {
                        break;
                    }
                    if (state[x + j * k][y + i * k] == player && passedOpponent) {
                        piecesToFlip = true;
                        break;
                    }
                    else if (state[x + j * k][y + i * k] == player % 2 + 1) {
                        passedOpponent = true;
                        k++;
                    }
                }

                if (piecesToFlip) {

                    state[x][y] = player;

                    for (int h = 1; h <= k; h++) {
                        state[x + j * h][y + i * h] = player;
                    }

                    legalAtLeastOnce = true;
                }
            }
        }

        return legalAtLeastOnce;
    }
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int[][] getState() {
        return state;
    }

    public void setState(int[][] state) {
        this.state = state;
    }
}